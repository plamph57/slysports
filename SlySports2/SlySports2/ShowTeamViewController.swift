//
//  ShowTeamViewController.swift
//  SlySports2
//
//  Created by David McCallum on 23/04/2019.
//  Copyright © 2019 Macca Media. All rights reserved.
//

import UIKit

protocol ChooseTeam {
    func teamChosen()
}

class ShowTeamViewController: UIViewController {
    
    var team: Team!
    
    var delegate: ChooseTeam!
    var teamImage: UIImage?
    
    @IBOutlet weak var largeTeamBadge: UIImageView!
    @IBOutlet weak var largeTeamName: UILabel!
    @IBOutlet weak var teamChosenButton: UIButton!
    @IBOutlet weak var chooseTeamCons: NSLayoutConstraint!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        largeTeamName.text = team.name
        largeTeamName.alpha = 0
        largeTeamBadge.image = teamImage
        chooseTeamCons.constant -= view.bounds.width
        teamChosenButton.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        largeTeamBadge.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.2, options: .curveEaseInOut, animations: { [weak self] in
            
            self?.largeTeamBadge.transform = .identity
            
        })
        UIView.animate(withDuration: 1, delay: 0.5, options: .beginFromCurrentState, animations: {
            self.chooseTeamCons.constant += self.view.bounds.width
            self.teamChosenButton.alpha = 1.0
            self.largeTeamName.alpha = 1.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    
    }

    @IBAction func teamChosenTapped(_ sender: Any) {
        delegate.teamChosen()
        navigationController?.popViewController(animated: true)
        
    }
  
}

