//
//  TeamsTableTableViewController.swift
//  SlySports2
//
//  Created by David McCallum on 23/04/2019.
//  Copyright © 2019 Macca Media. All rights reserved.
//

import UIKit

protocol TeamCellDelegate: class {
    func didDownload(image: UIImage?, from url: URL)
}

class TeamCell: UITableViewCell {
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var badgeImage: UIImageView!
    
    weak var delegate: TeamCellDelegate?
    
    func configure(with team: Team) {
        teamNameLabel.text = team.name
    }
    
     func downloadImage(url: String) {
        
        // create url session
        // use badgeURL url
        let imageUrl = URL(string: url)!
        
        let session = URLSession(configuration: .default)
        // create image from data
        let dataTask = session.dataTask(with: imageUrl) {[weak self] (data, response, error) in
            guard let data = data else {return}
            let image = UIImage(data: data)
            DispatchQueue.main.async {
                self?.badgeImage.image = image
                // Want cell to be in charge of its own session
                self?.delegate?.didDownload(image: image, from: imageUrl)
            }
        }
        dataTask.resume()
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        backgroundColor = .white
        badgeImage.image = nil
    }
    
}

class TeamsTableTableViewController: UITableViewController {
    
    var selectedTeam: Team?
    var teams: [Team] = []
    var imageCache = NSCache<NSString, UIImage>()

    override func viewDidLoad() {
        super.viewDidLoad()
        getTeams()
    }
    
    

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamCell", for: indexPath) as!
        TeamCell
        cell.delegate = self
        
        let team = teams[indexPath.row]
        cell.positionLabel.text = "\(indexPath.row + 1)."
        cell.configure(with: team)
        
        if let image = imageCache.object(forKey: team.badgeURL as NSString) {
            cell.badgeImage?.image = image
        } else {
            cell.downloadImage(url: team.badgeURL)
        }
        
        if let selectedTeam = selectedTeam,
            teams[indexPath.row] == selectedTeam {
            cell.backgroundColor = .init(displayP3Red: 255, green: 215, blue: 0, alpha: 1)
        }

        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedTeam = teams[indexPath.row]
        let selectionVC = storyboard?.instantiateViewController(withIdentifier: "showTeamView") as! ShowTeamViewController
        selectionVC.delegate = self
        selectionVC.team = selectedTeam
        
        navigationController?.pushViewController(selectionVC, animated: true)
        
        if let badge = selectedTeam?.badgeURL as NSString?,
            let image = imageCache.object(forKey: badge) {
            selectionVC.teamImage = image
        }
    }
    
    func getTeams() {
        let urlString = "https://gist.githubusercontent.com/mccallum1987/e8eb14d74c5f530620fb1024ee7cf686/raw/729d7b9b37ed2d3b9a538d84fd99c2a4038e102b/teamData.json"
        let url = URL(string: urlString)!
        
        let urlSession = URLSession(configuration: .default)
        
        let dataTask = urlSession.dataTask(with: url) { [weak self] (data, response, error) in
            
            if let downloadedData = data {
                
                if let downloadedTeams = try? JSONDecoder().decode([Team].self, from: downloadedData) {
                    DispatchQueue.main.async {
                        self?.teams = downloadedTeams
                        self?.tableView.reloadData()
                    }
                }
            }
        }
        
        dataTask.resume()
    }
    
}

extension TeamsTableTableViewController: ChooseTeam {
    func teamChosen() {
        tableView.reloadData()
    }
}

extension TeamsTableTableViewController: TeamCellDelegate {
    func didDownload(image: UIImage?, from url: URL) {
        guard let image = image else { return }
        imageCache.setObject(image, forKey: url.absoluteString as NSString)
    }
}



