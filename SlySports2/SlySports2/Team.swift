//
//  Team.swift
//  SlySports2
//
//  Created by David McCallum on 24/04/2019.
//  Copyright © 2019 Macca Media. All rights reserved.
//

import Foundation

struct Team: Decodable, Equatable {
    
    var name: String
    var badgeURL: String
    
}
